## Objective
- Learn Tasking.Like What is tasking,How to task,etc.
- Learn how to name projects, methods,classes,etc.
- Learn Git basic knowledge.
- Practice Tasking and coding.

## Reflective
All of the above courses have been useful for me.


## Interpretive
Because the knowledge taught in the above course plays a very important role in coding in our daily life.
This knowledge not only helps us code,but also helps others read or revise our code.

## Decisional
This knowledge is very important so I decided that I should continue to consolidate it in my spare time.


