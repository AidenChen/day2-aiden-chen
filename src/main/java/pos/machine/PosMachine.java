package pos.machine;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String, Long> barcodesOccurMap = countBarcodesOccur(barcodes);
        List<ReceiptItem> receiptItems = barcodesOccurMapToReceiptItems(barcodesOccurMap);
        setReceiptItemsSubTotal(receiptItems);
        Receipt receipt = receiptItemsToReceipt(receiptItems);
        return receiptToStr(receipt);
    }


    public Map<String, Long> countBarcodesOccur(List<String> barcodes) {
        return barcodes.stream().collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }


    public List<ReceiptItem> barcodesOccurMapToReceiptItems(Map<String, Long> barcodesOccurMap) {
        List<ReceiptItem> receiptItems = new ArrayList<>(barcodesOccurMap.size());
        barcodesOccurMap.keySet().forEach(barcode -> {
            ReceiptItem receiptItem = new ReceiptItem();
            Item item = this.getItemByBarcode(barcode);
            receiptItem.setBarcode(barcode);
            receiptItem.setName(item.getName());
            receiptItem.setPrice(item.getPrice());
            receiptItem.setQuantity(Math.toIntExact(barcodesOccurMap.get(barcode)));
            receiptItems.add(receiptItem);
        });
        return receiptItems.stream().sorted(Comparator.comparing(ReceiptItem::getBarcode)).collect(Collectors.toList());
    }

    public Item getItemByBarcode(String barcode) {
        List<Item> items = ItemsLoader.loadAllItems();
        for (Item item : items) {
            if (barcode.equals(item.getBarcode())) {
                return item;
            }
        }
        return null;
    }

    public void setReceiptItemsSubTotal(List<ReceiptItem> receiptItems) {
        receiptItems.forEach(receiptItem -> receiptItem.setSubtotal(receiptItem.getQuantity() * receiptItem.getPrice()));
    }

    public Receipt receiptItemsToReceipt(List<ReceiptItem> receiptItems) {
        Receipt receipt = new Receipt();
        int total = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            total += receiptItem.getSubtotal();
        }
        receipt.setReceiptItems(receiptItems);
        receipt.setTotal(total);
        return receipt;
    }

    private String receiptToStr(Receipt receipt) {
        StringBuilder receiptStr = new StringBuilder();
        receiptStr.append("***<store earning no money>Receipt***\n");
        for (int i = 0; i < receipt.getReceiptItems().size(); i++) {
            ReceiptItem receiptItem = receipt.getReceiptItems().get(i);
            receiptStr.append(String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)\n",
                    receiptItem.getName(), receiptItem.getQuantity(), receiptItem.getPrice(), receiptItem.getSubtotal()));
        }
        receiptStr.append("----------------------\n");
        receiptStr.append(String.format("Total: %d (yuan)\n", receipt.getTotal()));
        receiptStr.append("**********************");
        return receiptStr.toString();
    }



}
